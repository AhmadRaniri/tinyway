Berikut adalah beberapa hal yg mungkin menjadi fokus utama untuk ke depan :

- menambah fitur untuk mengatur gambar background.
- segera memindah fokus jendela ketika ada jendela yg ditutup.
- membuat lxqt panel dan pcmanfm-qt (ketika sebagai wallpaper setter) tidak terindeks ketika melakukan cycle jendela.
- membuat jendela `imv` bisa dipindah dan diresize lewat sudut jendela.
- menyederhanakan fungsi yang berulang.
- menerapkan _popup unconstrain_.
