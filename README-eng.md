## What's this ?

### Tinywl with some additional features. 

The additional features are :

<details>
  <summary>Additional features.</summary>

- Tap to click support, for touchpad laptop users.
- Close window function, to close window via keybind.
- Window manipulation function via keybind.
    - Move window to any direction (left, right, up, down and center).
    - Resize window to any direction (left, right, up, down and half width and height of screen).
    - Maximize window.
    - Snap window to any direction (left, right, up, down and center).
    - Pseudo minimize.
    - Fullscreen window.
    - One-third split layout.
- launcher (wofi).
- Screencopy protokol to support screenshot.
- Primary selection protocol.
- Simple background color.
- Switch to virtual terminal.
- Simple exclusive zone to place a bar / panel at bottom or top, toggleable. The only tested and working panel is lxqt-panel.
- Simple rule for a specific app_id / title, e.g for lxqt-panel.
- Screenshot via external tool (grim), for fullscreen screenshot and focused-window screenshot.
- Simple show desktop.
- Send to desktop, send all minimized windows back to desktop / surface.
- Cursor theme option, we can set the cursor theme using these envs, **XCURSOR_THEME** (cursor theme) and **XCURSOR_SIZE** (cursor size).
- Window maximizing and minimizing via decoration button. 
- Support for simple virtual keyboard protocol, allowing us to use tools that need virtual keyboard protocol (e.g. wtype).

</details>

By default, The available shortcut are :

<details>
  <summary>Shortcut.</summary>

- mod + enter : Open terminal (foot).
- mod + p : Open launcher (wofi).
- mod + h : maximize window vertically to left.
- mod + l : maximize window vertically to right.
- mod + f : maximize window.
- mod + shift + f : fullscreen window.
- mod + esc : close / quit compositor.
- mod + q : close / kill window.
- mod + j : send window to the lowest / minimize.
- mod + k : send window to the highest position.
- mod + {left, right, up, down} : move window to left, right, up, down.
- mod + shift + {l, h, k, j} : resize window to left, right, up, down.
- mod + shift + {r, e, t, w} : move window to most left, right, up, down of the screen.
- mod + Tab : cycle window.
- mod + {F1,F2,..F5} : switch to virtual terminal 1 to 5.
- mod + z : toggle exclusive zone.
- mod + shift + z : toggle exclusive zone position (top / bottom).
- mod + w : screenshot for the focused-window.
- mod + shift + p : screenshot for fullscreen.
- mod + {1, 2, 3} : resize window one-third of screen width and place it to left, center or right.
- Logo + shift + j : minimize all windows / show desktop.
- Logo + shift + k : send all minimized windows back to desktop / surface.

</details>

## Dependency.

TinyWay has dependency on wlroots library, check on wlroots at commit [717ded9b](https://gitlab.freedesktop.org/wlroots/wlroots/-/commit/717ded9b).

## How to *build*, *install* and *run*.

These are steps to *build*, *install* and *run*.

<details>
  <summary>Click to expand.</summary>

1. Adjust the setting or configuration at config_tinywl.h !
1. Adjust the keybind / shortcut at tinywl.c, check at handle_keybinding function (optional) !
1. Build with `make` command !
1. Install with `make install` ! TinyWay will be installed at `$HOME/.local/bin/`.
1. TinyWay can be ran automatcally using `$HOME/.local/bin/tinywl` command.
1. To make screenshot work properly, run TinyWay with this command !

    ```
    $ $HOME/.local/bin/tinywl > ~/.cache/tiny_info
    ```

1. Don't Forget to copy the screenshot script "misc/ss_window" to your $PATH !
1. We can run TinyWay using custom cursor theme using these envs, **XCURSOR_THEME** (cursor theme) and **XCURSOR_SIZE** (cursor size).

    ```
    $ XCURSOR_THEME=your_cursor_theme XCURSOR_SIZE=cursor_size $HOME/.local/bin/tinywl > ~/.cache/tiny_info
    ```

</details>

